import React from 'react';
import { StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';

import Home from './screens/Home';
import BrowserCategories from './screens/BrowserCategories';
import Browser from './screens/Browser';
import Quiz from './screens/Quiz';
import Info from './screens/Info';

import configureStore from './redux/configureStore';

import I18n from './I18n';

I18n.initialize();

const Navigator = StackNavigator({
  Home: { screen: Home },
  Quiz: { screen: Quiz },
  BrowserCategories: { screen: BrowserCategories },
  Browser: { screen: Browser },
  Info: { screen: Info },
});

const store = configureStore();

const App = () => (
  <Provider store={store}>
    <Navigator />
  </Provider>
);

export default App;
