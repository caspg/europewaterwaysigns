import I18n from 'react-native-i18n';

import locales from './locales';

function initialize() {
  I18n.fallbacks = true;
  I18n.translations = locales;
}

export default { initialize };
