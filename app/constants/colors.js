export default {
  lightBlue: '#03A9F4',
  lightBlue50: '#E1F5FE',
  lightBlue600: '#039BE5',

  grey200: '#EEEEEE',
  grey400: '#BDBDBD',
  grey600: '#757575',
  grey700: '#616161',

  green: '#4CAF50',

  red: '#F44336',
};
