export default [
  { key: 'prohibitory_signs', signsCount: '21' },
  { key: 'mandatory_signs', signsCount: '16' },
  { key: 'restrictive_signs', signsCount: '5' },
  { key: 'recommendatory_signs', signsCount: '4' },
  { key: 'informative_signs', signsCount: '27' },
];
