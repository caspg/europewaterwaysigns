import en from './translations/en.json';
import pl from './translations/pl.json';

export default {
  en,
  pl,
};
