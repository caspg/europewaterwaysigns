import { createStore, combineReducers } from 'redux';

import currentQuizReducer from './domains/currentQuiz/currentQuizReducer';

const rootReducer = combineReducers({
  currentQuiz: currentQuizReducer,
});

const configureStore = initialState => (
  createStore(
    rootReducer,
    initialState,
  )
);

export default configureStore;
