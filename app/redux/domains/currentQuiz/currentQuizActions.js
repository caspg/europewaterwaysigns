import prepareQuestions from '../../../utils/prepareQuestions';

const SET_QUESTIONS = 'currentQuiz.SET_QUESTIONS';
const ADD_ANSWER = 'currentQuiz.ADD_ANSWER';
const FINISH_QUIZ = 'currentQuiz.FINISH_QUIZ';
const UPDATE_CURRENT_INDEX = 'currentQuiz.UPDATE_CURRENT_INDEX';
const RESTART_QUIZ = 'currentQuiz.RESTART_QUIZ';
const PREVIEW_ANSWERS = 'currentQuiz.PREVIEW_ANSWERS';

function setQuestions() {
  const questions = prepareQuestions();

  return { type: SET_QUESTIONS, questions };
}

function addAnswer(questionIndex, answer, isCorrect) {
  return { type: ADD_ANSWER, questionIndex, answer, isCorrect };
}

function finishQuiz() {
  return { type: FINISH_QUIZ };
}

function updateCurrentIndex(newValue) {
  return { type: UPDATE_CURRENT_INDEX, newValue };
}

function restartQuiz() {
  const questions = prepareQuestions();

  return { type: RESTART_QUIZ, questions };
}

function previewAnswers() {
  return { type: PREVIEW_ANSWERS };
}

export {
  SET_QUESTIONS,
  ADD_ANSWER,
  FINISH_QUIZ,
  UPDATE_CURRENT_INDEX,
  RESTART_QUIZ,
  PREVIEW_ANSWERS,
  setQuestions,
  addAnswer,
  finishQuiz,
  updateCurrentIndex,
  restartQuiz,
  previewAnswers,
};
