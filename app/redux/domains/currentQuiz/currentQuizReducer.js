import {
  SET_QUESTIONS,
  ADD_ANSWER,
  FINISH_QUIZ,
  UPDATE_CURRENT_INDEX,
  RESTART_QUIZ,
  PREVIEW_ANSWERS,
} from './currentQuizActions';

const initialState = {
  currentQuestionIndex: 0,
  questions: [],
  answers: {},
  isFinished: false,
  isPreviewing: false,
};

function mergeObjects(currentObject, newObject) {
  return Object.assign({}, currentObject, newObject);
}

function currentQuizReducer(state = initialState, action) {
  switch (action.type) {
    case SET_QUESTIONS:
      return mergeObjects(state, { questions: action.questions });

    case ADD_ANSWER: {
      const updatedAnswers = mergeObjects(state.answers, {
        [action.questionIndex]: {
          answer: action.answer,
          isCorrect: action.isCorrect,
        },
      });

      return mergeObjects(state, { answers: updatedAnswers });
    }

    case FINISH_QUIZ:
      return mergeObjects(state, {
        isFinished: true,
        isPreviewing: false,
      });

    case UPDATE_CURRENT_INDEX:
      return mergeObjects(state, { currentQuestionIndex: action.newValue });

    case RESTART_QUIZ:
      return {
        questions: action.questions,
        currentQuestionIndex: 0,
        answers: {},
        isFinished: false,
        isPreviewing: false,
      };

    case PREVIEW_ANSWERS:
      return mergeObjects(state, { isPreviewing: true, currentQuestionIndex: 0 });

    default:
      return state;
  }
}

export default currentQuizReducer;
