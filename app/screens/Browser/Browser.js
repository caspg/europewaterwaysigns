import React, { PropTypes, Component } from 'react';
import { FlatList } from 'react-native';

import signs from '../../data/signs';
import colors from '../../constants/colors';
import BrowserItem from './components/BrowserItem';

class Browser extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      state: PropTypes.shape({
        params: PropTypes.shape({
          categoryTitle: PropTypes.string.isRequired,
          category: PropTypes.shape({
            key: PropTypes.string.isRequired,
          }).isRequired,
        }).isRequired,
      }),
    }).isRequired,
  };

  static navigationOptions = {
    title: ({ state }) => state.params.categoryTitle,
    header: {
      tintColor: 'white',
      style: {
        backgroundColor: colors.lightBlue,
      },
    },
  };

  constructor(props) {
    super(props);

    const { params: { category } } = this.props.navigation.state;
    this.state = { dataSource: signs[category.key] };
  }

  render() {
    return (
      <FlatList
        data={this.state.dataSource}
        renderItem={({ item }) => <BrowserItem sign={item} />}
      />
    );
  }
}

export default Browser;
