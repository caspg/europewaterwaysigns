import React, { PropTypes } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../../constants/colors';

const propTypes = {
  sign: PropTypes.shape({
    key: PropTypes.string.isRequired,
  }).isRequired,
};

function BrowserItem({ sign }) {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.signImage}
          source={{ uri: sign.key }}
        />
      </View>

      <Text style={styles.text}>
        {I18n.t(`signs.${sign.key}`)}
      </Text>
    </View>
  );
}

BrowserItem.propTypes = propTypes;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: colors.grey200,
    paddingTop: 50,
    paddingBottom: 30,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  imageContainer: {
    height: 120,
    width: 120,
    marginBottom: 20,
  },
  signImage: {
    flex: 1,
    resizeMode: 'contain',
    width: null,
    height: null,
  },
  text: {
    color: '#444',
    marginTop: 10,
    marginBottom: 10,
  },
});

export default BrowserItem;
