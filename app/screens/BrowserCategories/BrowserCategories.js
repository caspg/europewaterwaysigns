import React, { PropTypes, Component } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../constants/colors';
import categories from '../../data/categories';
import CategoryButton from './components/CategoryButton';

class BrowserCategories extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      setParams: PropTypes.func.isRequired,
    }).isRequired,
  }

  static navigationOptions = {
    title: ({ state }) => state.params.headerTitle,
    header: {
      tintColor: 'white',
      style: {
        backgroundColor: colors.lightBlue,
      },
    },
  }

  render() {
    const { navigation: { navigate } } = this.props;
    const renderCategories = () => (
      categories.map((category) => {
        const categoryTitle = I18n.t(`BrowserCategories.categories.${category.key}`);

        return (
          <CategoryButton
            key={category.key}
            text={categoryTitle}
            signsCount={category.signsCount}
            onPress={() => navigate('Browser', { category, categoryTitle })}
          />
        );
      })
    );

    return (
      <ScrollView style={styles.container}>
        {renderCategories()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default BrowserCategories;
