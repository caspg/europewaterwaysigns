import React, { PropTypes } from 'react';
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from 'react-native-i18n';

import colors from '../../../constants/colors';

const propTypes = {
  text: PropTypes.string.isRequired,
  signsCount: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const CategoryButton = ({ text, signsCount, onPress }) => (
  <TouchableHighlight
    onPress={onPress}
    underlayColor={colors.grey200}
    style={styles.container}
  >

    <View style={styles.wrapper}>
      <View>
        <Text style={styles.text}>
          {text}
        </Text>

        <Text style={styles.secondaryText}>
          {`${I18n.t('BrowserCategories.total_signs')} ${signsCount}`}
        </Text>
      </View>

      <Icon name="chevron-right" color={colors.grey700} size={20} style={styles.icon} />
    </View>

  </TouchableHighlight>
);

CategoryButton.propTypes = propTypes;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 10,
    height: 75,
    borderBottomWidth: 1,
    borderBottomColor: colors.grey200,
    paddingHorizontal: 20,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: '#444',
    fontSize: 15,
  },
  secondaryText: {
    marginTop: 5,
    fontSize: 12,
  },
  icon: {
    alignSelf: 'center',
  },
});

export default CategoryButton;
