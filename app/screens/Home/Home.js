import React, { Component, PropTypes } from 'react';
import { ScrollView, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from 'react-native-i18n';

import colors from '../../constants/colors';
import HomeButton from './components/HomeButton';
import HomeInfoButton from './components/HomeInfoButton';

class Home extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
    }).isRequired,
  }

  static navigationOptions = {
    header: {
      visible: false,
    },
  }

  render() {
    const { navigate } = this.props.navigation;

    const browserCategoriesTitle = I18n.t('BrowserCategories.header_title');

    return (
      <ScrollView style={styles.container}>

        <View style={styles.HomeInfoButtonContainer}>
          <HomeInfoButton
            onPress={() => navigate('Info', { headerTitle: I18n.t('Info.header_title') })}
          />
        </View>

        <View style={styles.textContainer}>
          <Icon name="directions-boat" style={styles.iconBoat} />

          <Text style={styles.mainText}>
            {I18n.t('Home.main_text')}
          </Text>
        </View>

        <View style={styles.buttonContainer}>
          <HomeButton
            text={I18n.t('Home.browse')}
            onPress={() => navigate('BrowserCategories', { headerTitle: browserCategoriesTitle })}
          />

          <HomeButton
            text={I18n.t('Home.learn')}
            onPress={() => navigate('Quiz')}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightBlue,
  },
  HomeInfoButtonContainer: {
    alignSelf: 'flex-end',
  },
  textContainer: {
    marginTop: 20,
    padding: 10,
  },
  mainText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  buttonContainer: {
    flex: 1,
    marginTop: 40,
    marginBottom: 60,
  },
  iconBoat: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 70,
    marginBottom: 30,
  },
});

export default Home;
