import React, { PropTypes } from 'react';
import { TouchableHighlight, Text, StyleSheet } from 'react-native';

import colors from '../../../constants/colors';

const propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const HomeButton = ({ text, onPress }) => (
  <TouchableHighlight
    onPress={onPress}
    underlayColor={colors.grey200}
    style={styles.container}
  >
    <Text style={styles.text}>
      {text}
    </Text>
  </TouchableHighlight>
);

HomeButton.propTypes = propTypes;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 25,
    padding: 20,
    minHeight: 70,
    justifyContent: 'center',
    borderRadius: 2,
    elevation: 5,
  },
  text: {
    fontSize: 16,
    color: '#444',
  },
});

export default HomeButton;

