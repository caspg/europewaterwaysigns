import React, { PropTypes } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

HomeInfoButton.propTypes = {
  onPress: PropTypes.func.isRequired,
};

function HomeInfoButton({ onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
    >
      <Icon name="info-outline" color="white" style={styles.icon} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  icon: {
    fontSize: 25,
  },
});

export default HomeInfoButton;

