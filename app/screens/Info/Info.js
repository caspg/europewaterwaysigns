import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import colors from '../../constants/colors';

class Info extends Component {
  static navigationOptions = {
    title: ({ state }) => state.params.headerTitle,
    header: {
      tintColor: 'white',
      style: {
        backgroundColor: colors.lightBlue,
      },
    },
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>View</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
});

export default Info;
