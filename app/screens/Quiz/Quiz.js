import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';

import colors from '../../constants/colors';
import HeaderRefreshButton from './components/HeaderRefreshButton';
import Question from './components/Question';
import QuizEnd from './components/QuizEnd';

import {
  setQuestions,
  addAnswer,
  finishQuiz,
  updateCurrentIndex,
  restartQuiz,
  previewAnswers,
} from '../../redux/domains/currentQuiz/currentQuizActions';

class Quiz extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      setParams: PropTypes.func.isRequired,
    }).isRequired,
    dispatch: PropTypes.func.isRequired,
    questions: PropTypes.array.isRequired, // eslint-disable-line react/forbid-prop-types
    answers: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
    isPreviewing: PropTypes.bool.isRequired,
    isFinished: PropTypes.bool.isRequired,
    currentQuestionIndex: PropTypes.number.isRequired,
  };

  static navigationOptions = {
    header: ({ state }) => ({
      tintColor: 'white',
      style: {
        backgroundColor: colors.lightBlue,
      },
      right: (state.params && state.params.finished) ?
        null :
        <HeaderRefreshButton />,
    }),
  }

  componentDidMount() {
    if (this.props.questions.length === 0) {
      this.props.dispatch(
        setQuestions()
      );
    }
  }

  handleFinishQuiz() {
    this.props.dispatch(
      finishQuiz()
    );

    this.props.navigation.setParams({ finished: true });
  }

  handleAddAnswer = (questionIndex, answer, isCorrect) => {
    if (this.props.isPreviewing) return;

    this.props.dispatch(
      addAnswer(questionIndex, answer, isCorrect)
    );
  }

  handleUpdateCurrentIndex = (change) => {
    const { currentQuestionIndex, questions, dispatch } = this.props;
    const newValue = currentQuestionIndex + change;

    if (newValue < 0) return;

    if (newValue > (questions.length - 1)) {
      this.handleFinishQuiz();
    } else {
      dispatch(
        updateCurrentIndex(newValue)
      );
    }
  }

  handleGoHome = () => {
    this.props.navigation.navigate('Home');
  };

  handleRestartQuiz = () => {
    this.props.navigation.setParams({ finished: false });

    this.props.dispatch(
      restartQuiz()
    );
  };

  handlePreviewAnswers = () => {
    this.props.dispatch(
      previewAnswers()
    );
  }

  render() {
    const { currentQuestionIndex, questions, answers, isPreviewing, isFinished } = this.props;

    const currentQuestion = questions[currentQuestionIndex];
    const questionIndicator = `${currentQuestionIndex + 1}/${questions.length}`;
    const givenAnswer = answers[currentQuestionIndex];

    const questionComponent = () => (
      <Question
        question={currentQuestion}
        questionIndicator={questionIndicator}
        questionIndex={currentQuestionIndex}
        givenAnswer={givenAnswer}
        addAnswer={this.handleAddAnswer}
        updateCurrentIndex={this.handleUpdateCurrentIndex}
        isPreviewing={isPreviewing}
      />
    );

    const quizEndComponent = () => (
      <QuizEnd
        questions={questions}
        answers={answers}
        goHome={this.handleGoHome}
        restartQuiz={this.handleRestartQuiz}
        previewAnswers={this.handlePreviewAnswers}
      />
    );

    if (this.props.questions.length === 0) {
      return <ActivityIndicator animating size="large" style={{ marginTop: 100 }} />;
    }

    return (
      (isFinished && !isPreviewing) ?
        quizEndComponent() :
        questionComponent()
    );
  }
}

const mapStateToProps = state => ({
  currentQuestionIndex: state.currentQuiz.currentQuestionIndex,
  questions: state.currentQuiz.questions,
  answers: state.currentQuiz.answers,
  isFinished: state.currentQuiz.isFinished,
  isPreviewing: state.currentQuiz.isPreviewing,
});

export default connect(mapStateToProps)(Quiz);
