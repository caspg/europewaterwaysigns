import React, { PropTypes } from 'react';
import { Alert, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import I18n from 'react-native-i18n';

import { restartQuiz } from '../../../redux/domains/currentQuiz/currentQuizActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function HeaderRefreshButton({ dispatch }) {
  const restart = () => dispatch(restartQuiz());

  const onPress = () =>
    Alert.alert(
      null, I18n.t('Quiz.restart_question'), [
        { text: I18n.t('common.no') },
        { text: I18n.t('common.yes'), onPress: restart },
      ],
    );

  return (
    <TouchableOpacity onPress={onPress}>
      <Icon name="refresh" color="white" size={24} style={styles.icon} />
    </TouchableOpacity>
  );
}

HeaderRefreshButton.propTypes = propTypes;

const styles = StyleSheet.create({
  icon: {
    marginRight: 15,
  },
});

export default connect()(HeaderRefreshButton);
