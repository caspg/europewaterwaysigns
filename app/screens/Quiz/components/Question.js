import React, { PropTypes } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';

import QuestionHeader from './QuestionHeader';
import QuestionAnswer from './QuestionAnswer';
import QuestionBottomNav from './QuestionBottomNav';

const propTypes = {
  question: PropTypes.shape({
    key: PropTypes.string.isRequired,
    correctAnswer: PropTypes.string.isRequired,
    answers: PropTypes.array.isRequired,
  }).isRequired,
  questionIndicator: PropTypes.string.isRequired,
  questionIndex: PropTypes.number.isRequired,
  addAnswer: PropTypes.func.isRequired,
  updateCurrentIndex: PropTypes.func.isRequired,
  givenAnswer: PropTypes.shape({
    isCorrect: PropTypes.bool,
    answer: PropTypes.string,
  }),
  isPreviewing: PropTypes.bool.isRequired,
};

const defaultProps = {
  givenAnswer: { answer: '' },
};

/* eslint-disable react/no-array-index-key */
function Question(props) {
  const {
    question,
    questionIndicator,
    questionIndex,
    addAnswer,
    givenAnswer,
    updateCurrentIndex,
    isPreviewing,
  } = props;

  return (
    <View style={styles.container}>
      <QuestionHeader
        signKey={question.key}
        questionIndicator={questionIndicator}
        isPreviewing={isPreviewing}
      />

      <ScrollView style={styles.answerContainer}>
        {
          question.answers.map((answerLabelKey, i) =>
            <QuestionAnswer
              key={i}
              answerLabelKey={answerLabelKey}
              addAnswer={addAnswer}
              questionIndex={questionIndex}
              givenAnswerValue={givenAnswer.answer}
              isCorrect={answerLabelKey === question.correctAnswer}
              isPreviewing={isPreviewing}
            />
          )
        }
      </ScrollView>

      <QuestionBottomNav
        questionIndex={questionIndex}
        updateCurrentIndex={updateCurrentIndex}
      />
    </View>
  );
}

Question.propTypes = propTypes;
Question.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  answerContainer: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
});

export default Question;
