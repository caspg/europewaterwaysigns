import React, { PropTypes } from 'react';
import { Vibration, TouchableOpacity, Text, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../../constants/colors';

const propTypes = {
  answerLabelKey: PropTypes.string.isRequired,
  addAnswer: PropTypes.func.isRequired,
  questionIndex: PropTypes.number.isRequired,
  givenAnswerValue: PropTypes.string.isRequired,
  isCorrect: PropTypes.bool.isRequired,
  isPreviewing: PropTypes.bool.isRequired,
};

function backgroundColor(isCorrect, givenAnswerValue, answerKey) {
  if (!givenAnswerValue) return 'white';
  if (givenAnswerValue === answerKey && isCorrect) return colors.green;
  if (givenAnswerValue === answerKey && !isCorrect) return colors.red;
  if (isCorrect) return colors.green;

  return 'white';
}

function containerStyle(isCorrect, givenAnswerValue, answerKey) {
  const dynamicStyle = StyleSheet.create({
    container: {
      backgroundColor: backgroundColor(isCorrect, givenAnswerValue, answerKey),
    },
  });

  return StyleSheet.flatten([styles.container, dynamicStyle.container]);
}

function QuestionAnswer({
  answerLabelKey, addAnswer, questionIndex, givenAnswerValue, isCorrect, isPreviewing,
}) {
  const handleOnPress = () => {
    if (!givenAnswerValue) {
      if (!isCorrect && !isPreviewing) {
        /**
         * TODO android only
         */
        Vibration.vibrate();
      }

      addAnswer(questionIndex, answerLabelKey, isCorrect);
    }
  };

  return (
    <TouchableOpacity
      onPress={handleOnPress}
      activeOpacity={0.5}
      style={containerStyle(isCorrect, givenAnswerValue, answerLabelKey)}
    >
      <Text style={styles.answerText}>
        {I18n.t(`signs.${answerLabelKey}`)}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    elevation: 1,
    padding: 18,
    margin: 20,
    marginTop: 10,
    marginBottom: 1,
    borderRadius: 2,
  },
});

QuestionAnswer.propTypes = propTypes;

export default QuestionAnswer;
