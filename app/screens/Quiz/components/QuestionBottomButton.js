import React, { PropTypes } from 'react';
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import colors from '../../../constants/colors';

QuestionBottomButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

function QuestionBottomButton({ onPress, icon, label }) {
  return (
    <TouchableHighlight
      style={styles.container}
      onPress={onPress}
      underlayColor={colors.lightBlue600}
    >
      <View>
        <Icon name={icon} color="white" style={styles.icon} />
        <Text style={styles.text}>{label}</Text>
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 40,
    paddingTop: 6,
    paddingBottom: 10,
  },
  text: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 12,
  },
  icon: {
    alignSelf: 'center',
    fontSize: 24,
  },
});

export default QuestionBottomButton;
