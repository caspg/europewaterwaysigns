import React, { PropTypes } from 'react';
import { View, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../../constants/colors';
import QuestionBottomButton from './QuestionBottomButton';

const propTypes = {
  updateCurrentIndex: PropTypes.func.isRequired,
  questionIndex: PropTypes.number.isRequired,
};

function renderBackButton(questionIndex, updateCurrentIndex) {
  if (questionIndex === 0) return <View />;

  return (
    <QuestionBottomButton
      onPress={() => updateCurrentIndex(-1)}
      icon="chevron-left"
      label={I18n.t('Quiz.previous')}
    />
  );
}

function QuestionBottomNav({ questionIndex, updateCurrentIndex }) {
  return (
    <View style={styles.container}>
      {renderBackButton(questionIndex, updateCurrentIndex)}

      <QuestionBottomButton
        onPress={() => updateCurrentIndex(1)}
        icon="chevron-right"
        label={I18n.t('Quiz.next')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightBlue,
    height: 56,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

QuestionBottomNav.propTypes = propTypes;

export default QuestionBottomNav;
