import React, { PropTypes } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../../constants/colors';

const propTypes = {
  signKey: PropTypes.string.isRequired,
  questionIndicator: PropTypes.string.isRequired,
  isPreviewing: PropTypes.bool.isRequired,
};

function QuestionHeader({ signKey, questionIndicator, isPreviewing }) {
  const previewing = isPreviewing ? `${I18n.t('Quiz.preview')} - ` : '';
  const headerText = `${previewing} ${questionIndicator}`;

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>
        {headerText}
      </Text>

      <View style={styles.imageContainer}>
        <Image
          style={styles.signImage}
          source={{ uri: signKey }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.lightBlue50,
  },
  headerText: {
    alignSelf: 'flex-end',
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 5,
    color: '#444',
  },
  imageContainer: {
    height: 150,
    width: 150,
    padding: 5,
    paddingBottom: 20,
  },
  signImage: {
    flex: 1,
    resizeMode: 'contain',
    width: null,
    height: null,
  },
});

QuestionHeader.propTypes = propTypes;

export default QuestionHeader;
