import React, { PropTypes } from 'react';
import { View, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import QuizEndHeader from './QuizEndHeader';
import QuizEndFlatButton from './QuizEndFlatButton';

const propTypes = {
  questions: PropTypes.array.isRequired,
  answers: PropTypes.object.isRequired,
  goHome: PropTypes.func.isRequired,
  restartQuiz: PropTypes.func.isRequired,
  previewAnswers: PropTypes.func.isRequired,
};

function QuizEnd({ questions, answers, goHome, restartQuiz, previewAnswers }) {
  const answerArray = Object.keys(answers).map(key => answers[key]);
  const questionsCount = questions.length;
  const correctAnswersCount = answerArray.filter(a => a.isCorrect).length;
  const scorePercent = Math.round((correctAnswersCount * 100) / questionsCount);

  return (
    <View style={styles.container}>
      <QuizEndHeader
        scorePercent={scorePercent}
        correctAnswersCount={correctAnswersCount}
        questionsCount={questionsCount}
      />

      <QuizEndFlatButton icon="home" text={I18n.t('Quiz.home')} onPress={goHome} />
      <QuizEndFlatButton icon="refresh" text={I18n.t('Quiz.start_again')} onPress={restartQuiz} />
      <QuizEndFlatButton icon="remove-red-eye" text={I18n.t('Quiz.preview_answers')} onPress={previewAnswers} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  answerContainer: {
    flex: 1,
  },
});

QuizEnd.propTypes = propTypes;

export default QuizEnd;
