import React, { PropTypes } from 'react';
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import colors from '../../../../constants/colors';

const propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

function QuizEndFlatButton({ text, onPress, icon }) {
  return (
    <TouchableHighlight
      onPress={onPress}
      underlayColor={colors.grey200}
      style={styles.container}
    >
      <View style={styles.wrapper}>
        <Text style={styles.text}>
          {text}
        </Text>

        <Icon name={icon} color={colors.grey700} size={20} style={styles.icon} />
      </View>
    </TouchableHighlight>
  );
}

QuizEndFlatButton.propTypes = propTypes;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    minHeight: 70,
    borderBottomWidth: 1,
    borderBottomColor: colors.grey200,
    paddingBottom: 1,
    paddingHorizontal: 20,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: '#444',
  },
  icon: {
    alignSelf: 'center',
  },
});

export default QuizEndFlatButton;
