import React, { PropTypes } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import I18n from 'react-native-i18n';

import colors from '../../../../constants/colors';

const propTypes = {
  scorePercent: PropTypes.number.isRequired,
  correctAnswersCount: PropTypes.number.isRequired,
  questionsCount: PropTypes.number.isRequired,
};

function QuizEndHeader(props) {
  return (
    <View style={styles.container}>

      <View style={styles.scoreRow}>
        <View style={styles.scoreContainer}>
          <Text style={styles.scoreText}>
            {I18n.t('Quiz.your_score')}
          </Text>
          <Text style={styles.scorePercent}>{props.scorePercent}%</Text>
        </View>

        <View style={{ justifyContent: 'center' }}>
          <Text style={styles.scroreNumbers}>
            {props.correctAnswersCount}/{props.questionsCount}
          </Text>
        </View>
      </View>

    </View>
  );
}

QuizEndHeader.propTypes = propTypes;

const scoreContainerSize = 150;
const textColor = '#444';
const scoreSize = 30;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.blueGrey700,
    height: 200,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.grey200,
    paddingBottom: 1,
  },
  scoreText: {
    color: textColor,
    alignSelf: 'center',
    marginBottom: 5,
    fontSize: 15,
  },
  scoreRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  scroreNumbers: {
    fontSize: scoreSize,
    color: textColor,
  },
  scoreContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: colors.grey200,
    borderRadius: scoreContainerSize / 2,
    width: scoreContainerSize,
    height: scoreContainerSize,
  },
  scorePercent: {
    color: textColor,
    fontSize: scoreSize,
    alignSelf: 'center',
  },
});

export default QuizEndHeader;
