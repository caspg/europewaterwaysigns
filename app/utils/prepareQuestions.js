import { shuffle, flatten } from 'lodash';

import categories from '../data/categories';
import signs from '../data/signs';

const NUMBER_OF_ANSWERS = 3;
const NUMBER_OF_QUESTIONS_TO_DISPLAY = 30;

/**
 *
 * @param {String} category
 * @param {String} correctAnswer
 * @return {Array.String} - array of possible answers
 */
function createAnswers(category, correctAnswer) {
  const allCategoryLabels = signs[category]
    .map(i => i.key)
    .filter(i => i !== correctAnswer);

  const shuffledLabels = shuffle(allCategoryLabels);
  const fakeAnswers = shuffledLabels.slice(0, NUMBER_OF_ANSWERS - 1);
  const answers = fakeAnswers.concat(correctAnswer);

  return shuffle(answers);
}

/**
 * @param {String} category
 * @returns {Array}
 */
function createQuestions(category) {
  return signs[category].map(sign => ({
    key: sign.key,
    correctAnswer: sign.key,
    answers: createAnswers('prohibitory_signs', sign.key),
  }));
}

/**
 * Prepares array of Questions
 * @returns {Array.Object}
 */
function prepareQuestions() {
  const questions = categories.map(category => createQuestions(category.key));

  return shuffle(
    flatten(questions)
  ).slice(0, NUMBER_OF_QUESTIONS_TO_DISPLAY);
}

export default prepareQuestions;
