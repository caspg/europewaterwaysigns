const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const MAIN_WIKI_URL = 'https://pl.wikipedia.org/wiki/Oznakowanie_%C5%9Br%C3%B3dl%C4%85dowych_dr%C3%B3g_wodnych';
const SIGN_PAGE_BASE_URL = 'https://pl.wikipedia.org';
const BASE_OUTPUT_PATH = './scripts/imageDownloader/images';

async function findSignPaths() {
  const response = await axios.get(MAIN_WIKI_URL);
  const $ = cheerio.load(response.data);
  const $tables = $('.wikitable');

  const signPaths = [];

  $tables.each((i, table) => {
    const $table = $(table);

    $table.children('tr').each((j, tr) => {
      const $tr = $(tr);

      if (j !== 0) {
        const $td = $($tr.children('td').get(1));
        const path = $td.children('a').first().attr('href');
        const symbol = $tr.children('th').text();

        if (path) {
          signPaths.push({ symbol, path });
        }
      }
    });
  });

  return signPaths;
}

function extractFileExtension(url) {
  const splittedUrl = url.split('.');

  return splittedUrl[splittedUrl.length - 1];
}

async function downloadSVG(url, fileName) {
  const response = await axios.get(`https:${url}`);
  const filePath = `${BASE_OUTPUT_PATH}/svg/${fileName}.svg`;

  fs.writeFile(filePath, response.data, { flag: 'w' }, (err) => {
    if (err) throw err;
  });
}

async function downloadPng(url, fileName) {
  const response = await axios.get(`https:${url}`, { responseType: 'stream' });

  const extension = extractFileExtension(url);
  const filePath = `${BASE_OUTPUT_PATH}/png/${fileName}.${extension}`;

  response.data.pipe(
    fs.createWriteStream(filePath, { flag: 'w' })
  );
}

async function downloadSignImages({ path, symbol }) {
  const response = await axios.get(`${SIGN_PAGE_BASE_URL}${path}`);
  const $ = cheerio.load(response.data);
  const svgUrl = $('#file a').attr('href');
  const pngUrl = $('#file a img').attr('src');
  const fileName = symbol.replace(' ', '_');

  downloadSVG(svgUrl, fileName);
  downloadPng(pngUrl, fileName);

  console.log('.');
}

async function downloadImages() {
  const signPaths = await findSignPaths();

  signPaths.forEach(downloadSignImages);
}

downloadImages();
